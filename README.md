#How to play SNAKEOFDOOM:

First of all, congratulations on selecting to play SNAKEOFDOOM. 

SNAKEOFDOOM is a game created for enjoyment, however a possible side effect of irritation and anger may occur. 


To begin enjoying this masterpiece, one first has to have all the necessary requirements downloaded. This includes: Java, Maven (if you use Macintosh), Git and a IDE of your choice. Known IDE's could be: VSCode, Eclipse, IntelliJ and more. 


##First you start off by installing Java. Follow one of this link to install the newest version of Java to your Windows computer:


-	Open your file explorer and navigate to "C:\Program Files"

-	If there isn't already one, create a folder with the name "Java"

-	Copy/paste this link to your web browser of choice:

-	https://jdk.java.net/17/ 
	
-	Choose: Windows / x64 (zip)

-	Unzip the file you downloaded and choose "extract". Select the "Java" folder you created earlier

-	In the search field of your windows computer, type "Show advanced settings" and press enter

-	Press "environment variables"

-	Under "User variables", press new. Variablename: JAVA_HOME, Variable value: C:\Program Files\Java\jdk-17.0.1

-	Under system variables: press "path". Press "new" and type: C:\Program Files\Java\jdk-17.0.1\bin

-	Press "ok" until all windows are closed

-	Open your terminal and copy/paste: java -version

-	If it says Java version 17 or newer, you're good to go


##If you're using Macintosh, follow these steps to download Java:


- 	Open a terminal on your macbook, copy and paste this line: 
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	
-	Enter your computer's password if necessary. 

-	Check whether the installment was successful by copy/pasting: brew --version

-	After this is done, make sure you have the newest version of HomeBrew by copy/pating: brew update

-	When this is done, you can copy/paste: brew install openjdk@17

-	Check if everything went smoothly by copy/pasting: java --version


#####Now, Java should be installed on your macbook.


For mac users, you'll also need a tool called "Maven" to create the wanted project.


##To install Maven, follow these simple steps:

-	In your terminal, copy/paste: brew install maven

-	To ensure that the installment was succesful, copy/paste: mvn --version

-	If the version is newer or similar to Maven 3.8, you should be good to go



##To install Eclipse (preferred IDE), follow this link:


-	https://www.eclipse.org/downloads/packages/release/2021-12/r/eclipse-ide-java-developers.
	
	
##To install GIT, follow this link to an amazingly detailed and well-produced tutorial:


-	https://www.youtube.com/watch?v=lw3Vz6WsomM


##Last step before the fun part begins, installing an ssh-key:


-	https://www.youtube.com/watch?v=rfK8snf5kIY



##To clone the project and start enjoying the game, follow these steps:


-	Open your terminal and choose a suited directory by typing "cd (suited directory)".


-	To check which directory you're in, type: "ls"

-	When you're in your directory of choice, copy/paste: git clone git@git.app.uib.no:Stinius.Langvik-Hansen/snakeofdoom.git

-	Open your Eclipse IDE

-	Press "Open file from file system" and select the cloned repository

-	Arrow down to the file named "src", then "game", then "SnakeMain.java".

-	When you've opened this file, press the green play button at the top of the program, and here we go

#Now that the boring part is over with, let's begin the fun part:)

-	Instruction are fairly easy and appear in the game, but you press space to start and utilize arrows to move

-	The goal is to eat apples and gain score






