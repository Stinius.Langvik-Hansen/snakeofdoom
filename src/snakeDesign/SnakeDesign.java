package snakeDesign;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;



public class SnakeDesign{
	
	SnakeDesignable snakeDesignable;
	
	public SnakeDesign(SnakeDesignable snakeDesignable) {
		this.snakeDesignable = snakeDesignable;
	}
	
	void drawGameOver(Graphics canvas) {
    		canvas.setColor(Color.RED);
    		canvas.setFont(new Font("Ink Free", Font.BOLD, 60));
    		canvas.drawString("GAME OVER!", ((SnakePanel.panelWidth) - canvas.getFontMetrics().stringWidth("GAME OVER!"))/2 , SnakePanel.panelHeight/2);
	}
	
	void drawWelcomeScreen(Graphics canvas) {
			canvas.setColor(Color.GREEN);
			canvas.setFont(new Font("SansSerif", Font.BOLD, 30));
			canvas.drawString("WELCOME TO SNAKEOFDOOM🐍!", ((SnakePanel.panelWidth) - canvas.getFontMetrics().stringWidth("WELCOME TO SNAKEOFDOOM🐍!"))/2, SnakePanel.panelHeight/2);
			
			canvas.setColor(Color.BLUE);
			canvas.setFont(new Font("SansSerif", Font.BOLD, 30));
			canvas.drawString("PRESS SPACE TO START", ((SnakePanel.panelWidth) - canvas.getFontMetrics().stringWidth("PRESS SPACE TO START"))/2 , 400);
			
			canvas.setFont(new Font("SansSerif", Font.BOLD, 30));
			canvas.drawString("USE ARROWS TO MOVE", ((SnakePanel.panelWidth) - canvas.getFontMetrics().stringWidth("USE ARROWS TO MOVE"))/2 , 500);
			
	}
	
}
