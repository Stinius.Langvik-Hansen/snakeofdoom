package snakeDesign;

import java.awt.Graphics;

public interface SnakeDesignable {

	
	/**
	 * 	// Initializes game
	 */
	public void initGame();
	
	/**
	 * 	// Ends game
	 */
	public void endGame();
	
	public void draw(Graphics g);
	
	public int getScore();
}
