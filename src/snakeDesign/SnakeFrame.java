package snakeDesign;

import javax.swing.JFrame;

public class SnakeFrame extends JFrame{
	
	private static final long serialVersionUID = 1L;

	SnakeDesignable snakeDesignable;
	
	public SnakeFrame() {
		this.add(new SnakePanel(new SnakeDesign(snakeDesignable)));
		this.setTitle("SNAKEOFDOOM🐍");
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
		this.setLocationRelativeTo(null);
	}
}
