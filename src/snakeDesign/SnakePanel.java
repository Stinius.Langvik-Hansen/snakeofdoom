package snakeDesign;

import java.awt.Color;



import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

import game.SnakeState;

public class SnakePanel extends JPanel implements ActionListener, SnakeDesignable {

	private static final long serialVersionUID = 1L;
	
	
	static final int panelWidth = 600;
	static final int panelHeight = 600;
	static final int squareSize = 25;
	static final int squares = (panelWidth * panelHeight) / squareSize;
	Random rand;
	int appleWidth;
	int appleHeight;
	int delay = 60;
	int snakeLength = 5;
	int applesEaten = 0;
	Timer timer;
	Directions direction = Directions.RIGHT;
	SnakeState snakeState = SnakeState.WELCOME;
	SnakeDesign SnakeDesign;
	final int x[] = new int[squares];
	final int y[] = new int[squares];

	public SnakePanel(SnakeDesign stateDesign) {
		this.SnakeDesign = stateDesign;
		rand = new Random();
		this.setPreferredSize(new Dimension(panelWidth, panelHeight));
		this.setBackground(Color.BLACK);
		this.setFocusable(true);
		this.addKeyListener(new mySnakeController());
		initGame();
	}
	

	// Initializes game
	public void initGame() {
		spawnApple();
		timer = new Timer(delay, this);
		timer.start();
	}


	public void endGame() {
		timer.stop();
		snakeState = SnakeState.GAME_OVER;
	}
	
	/**
	 * Checks if head of snake is on top of apple
	 * Updates tallies and spawns new apple
	 */
	private void checkApple() {
		if((x[0] == appleWidth) && (y[0] == appleHeight)) {
			applesEaten++;
			snakeLength++;
			spawnApple();
		}
	}


	public void paintComponent(Graphics g) {
		if(snakeState.equals(SnakeState.ACTIVE)) {
			super.paintComponent(g);
			draw(g);
		}
		if(snakeState.equals(SnakeState.WELCOME)) {
			SnakeDesign.drawWelcomeScreen(g);
		}
		if(snakeState.equals(SnakeState.GAME_OVER)) {
			SnakeDesign.drawGameOver(g);
		}
	}

	// Draw method to create apple and snake
	public void draw(Graphics g) {
		if(snakeState.equals(SnakeState.ACTIVE)) {
			g.setColor(Color.RED);
			g.fillOval(appleWidth, appleHeight, squareSize, squareSize);
	
			for (int i = 0; i < snakeLength; i++) {
				if (i == 0) {
					// Found these color combinations online
					g.setColor(new Color(102, 255, 102));
					g.fillRect(x[i], y[i], squareSize, squareSize);
				} else {
					// Found these color combinations online
					g.setColor(new Color(0, 102, 0));
					g.fillRect(x[i], y[i], squareSize, squareSize);
				}
				g.setColor(Color.RED);
				g.setFont(new Font("Ink Free", Font.BOLD, 30));
				g.drawString("Score = " + getScore(), 1, 30);
		}
		
	}
		else if (snakeState.equals(SnakeState.GAME_OVER)) {
			endGame();
		}

}

	// Spawns red apple at a random square
	private void spawnApple() {
		appleWidth = rand.nextInt((int) panelWidth / squareSize) * squareSize;
		appleHeight = rand.nextInt((int) panelHeight / squareSize) * squareSize;
	}
	
	/**
	 *  Iterates over length of snake. Moves each square towards the index before itself
	 *  
	 */
	private void moveSnake() {
		for (int i = snakeLength; i > 0; i--) {
			x[i] = x[i - 1];
			y[i] = y[i - 1];
		}
		
		if (direction.equals(Directions.RIGHT)) {
			x[0] = x[0] + squareSize;
		}
		if (direction.equals(Directions.LEFT)) {
			x[0] = x[0] - squareSize;
		}
		if (direction.equals(Directions.DOWN)) {
			y[0] = y[0] + squareSize;
		}
		if (direction.equals(Directions.UP)) {
			y[0] = y[0] - squareSize;
		}
		
}

	/**
	 * 
	 * @return the current gamestate of the snake game
	 */
	private SnakeState getSnakeState() {
		return snakeState;
	}

	/**
	 *  Checks if game is over
	 */
	private void checkLegality() {
		// Checks if head of snake collides with body of snake
		for (int i = snakeLength; i > 0; i--) {
			if ((x[0] == x[i] && y[0] == y[i])) {
				endGame();
			}
		}
		// Checks if head of snake collides with left border
		if (x[0] < 0) {
			endGame();
		}
		// Checks if head of snake collides with right border
		if (x[0] >= panelWidth) {
			endGame();
		}
		// Checks if head of snake collides with top border
		if (y[0] < 0) {
			endGame();
		}
		// Checks if head of snake collides with bottom border
		if (y[0] >= panelHeight) {
			endGame();
		}

	}
	
	public class mySnakeController extends KeyAdapter{
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			if (direction != Directions.RIGHT) {
				direction = Directions.LEFT;
			}
			break;
		case KeyEvent.VK_RIGHT:
			if (direction != Directions.LEFT) {
				direction = Directions.RIGHT;
			}
			break;
		case KeyEvent.VK_DOWN:
			if (direction != Directions.UP) {
				direction = Directions.DOWN;
			}
			break;
		case KeyEvent.VK_UP:
			if (direction != Directions.DOWN) {
				direction = Directions.UP;
			}
			break;
			
		case KeyEvent.VK_SPACE:
			if(getSnakeState().equals(SnakeState.WELCOME)) {
				snakeState = SnakeState.ACTIVE;
			}
		}
	}
}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (snakeState == SnakeState.ACTIVE) {
			moveSnake();
			checkApple();
			checkLegality();
		}
		repaint(); 
	}
	
	/**
	 * @return Score based on apples eaten
	 */
	public int getScore() {
		return applesEaten * 100;
	}
}
