package game;

public enum SnakeState {

	WELCOME,
	
	ACTIVE,
	
	PAUSED,
	
	GAME_OVER
}
